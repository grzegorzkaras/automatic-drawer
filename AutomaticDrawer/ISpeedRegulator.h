#pragma once

class ISpeedRegulator {
public:
  virtual int read(const int maxResolution) = 0;

  virtual ~ISpeedRegulator() {}
};