#pragma once

class IRangeRegulator {
public:
  virtual int read(const int maxResolution) = 0;

  virtual ~IRangeRegulator() {}
};