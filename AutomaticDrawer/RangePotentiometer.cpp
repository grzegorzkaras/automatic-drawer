#include "RangePotentiometer.h"

RangePotentiometer::RangePotentiometer(const int pin) : pin(pin) {
  pinMode(pin, INPUT);
}

int RangePotentiometer::read(const int maxResolution) {
  return map(analogRead(this->pin), 0, 1023, 0, maxResolution);
}
