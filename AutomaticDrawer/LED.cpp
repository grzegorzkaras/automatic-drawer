#include "LED.h"

LED::LED(const int pin) : pin(pin) {
  pinMode(pin, OUTPUT);
}

void LED::activate() {
  digitalWrite(this->pin, HIGH);
}

void LED::deactivate() {
  digitalWrite(this->pin, LOW);
}
