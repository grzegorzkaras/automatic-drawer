#pragma once

class IDeviceController {
public:
  virtual bool getStatus() = 0;

  virtual ~IDeviceController() {}
};