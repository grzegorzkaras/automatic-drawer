#pragma once

#include <Arduino.h>
#include <Servo.h>

#include "IPushPullMechanism.h"

class Servomechanism : virtual public IPushPullMechanism {
private:
  const int pin;

  Servo servo; 
  
  const int minPulseWidth;
  const int maxPulseWidth;
  
  const int minAngle;
  const int maxAngle;
  
  int additionalBackLimit;
  int additionalFrontLimit;
  
  const int initializeDelay;
  int delayPerAngle;
  
  int lastAngle;
  int angle;
  
  bool reverse;
  
  int getFrontLimit() const;
  int getBackLimit() const;
  
  bool canGoForward(const int angle) const;
  bool canGoBackward(const int angle) const;
  
  void writeAngle();

public:
  Servomechanism(const int pin, const int minAngle = 0, const int maxAngle = 180, const int delayPerAngle = 15, const int initializeDelay = 500, const int minPulseWidth = 544, const int maxPulseWidth = 2400);

  virtual void initialize();

  virtual int getMaxRangeLimit() const;
  
  virtual void setSpeed(const int speed);
  virtual void setRangeLimit(const int backLimit, const int frontLimit);
  
  virtual void sweepStep();
  virtual void controlledStep(const bool reverse);
};
