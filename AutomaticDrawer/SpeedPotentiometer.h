#pragma once

#include <Arduino.h>

#include "ISpeedRegulator.h"

class SpeedPotentiometer : virtual public ISpeedRegulator {
private:
  const int pin;

public:
  SpeedPotentiometer(const int pin);
  
  virtual int read(const int maxResolution = 1023);
};
