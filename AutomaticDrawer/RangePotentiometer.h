#pragma once

#include <Arduino.h>

#include "IRangeRegulator.h"

class RangePotentiometer : virtual public IRangeRegulator {
private:
  const int pin;

public:
  RangePotentiometer(const int pin);
  
  virtual int read(const int maxResolution = 1023);
};
