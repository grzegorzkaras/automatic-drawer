#pragma once

#include <Arduino.h>

#include "IDeviceController.h"

class ProximitySensor : virtual public IDeviceController {
private:
  const int pin;

public:
  ProximitySensor(const int pin);

  virtual bool getStatus();
};
