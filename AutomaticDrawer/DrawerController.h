#pragma once

#include <Arduino.h>

#include "IStatusIndicator.h"
#include "ISpeedRegulator.h"
#include "IRangeRegulator.h"
#include "IDeviceController.h"
#include "IPushPullMechanism.h"

class DrawerController {
private:
  IStatusIndicator* const statusIndicator;
  ISpeedRegulator* const speedRegulator;
  IRangeRegulator* const rangeRegulator;
  IDeviceController* const deviceController;
  IPushPullMechanism* const pushPullMechanism;

  int maxFrontRangeLimit;
  int maxDelayPerAngle;

  void updatePushPullMechanism();
  
public:
  DrawerController(IStatusIndicator* const statusIndicator, ISpeedRegulator* const speedRegulator, IRangeRegulator* const rangeRegulator, IDeviceController* const deviceController, IPushPullMechanism* const pushPullMechanism);
  ~DrawerController();

  void initialize();
  void update();

  void testModeUpdate();
};
