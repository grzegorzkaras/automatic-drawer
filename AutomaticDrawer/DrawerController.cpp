#include "DrawerController.h"

DrawerController::DrawerController(IStatusIndicator* const statusIndicator, ISpeedRegulator* const speedRegulator, IRangeRegulator* const rangeRegulator, IDeviceController* const deviceController, IPushPullMechanism* const pushPullMechanism)
  : statusIndicator(statusIndicator), speedRegulator(speedRegulator), rangeRegulator(rangeRegulator), deviceController(deviceController), pushPullMechanism(pushPullMechanism) {
  this->maxDelayPerAngle = 20;
  this->maxFrontRangeLimit = this->pushPullMechanism->getMaxRangeLimit();
}

DrawerController::~DrawerController() {
  delete this->statusIndicator;
  delete this->speedRegulator;
  delete this->rangeRegulator;
  delete this->deviceController;
  delete this->pushPullMechanism;
}

void DrawerController::updatePushPullMechanism() {
  int additionalFrontLimit = this->rangeRegulator->read(this->maxFrontRangeLimit);
  int delayPerAngle = this->speedRegulator->read(this->maxDelayPerAngle);

  this->pushPullMechanism->setRangeLimit(0, additionalFrontLimit);
  this->pushPullMechanism->setSpeed(delayPerAngle);
}

void DrawerController::initialize() {
  this->statusIndicator->activate();
  
  this->updatePushPullMechanism();
  this->pushPullMechanism->initialize();

  this->statusIndicator->deactivate();
}

void DrawerController::update() {
  bool reverse = deviceController->getStatus();

  if (!reverse) {
    this->statusIndicator->activate();
  } else { 
    this->statusIndicator->deactivate();
  }
  
  this->updatePushPullMechanism();
  this->pushPullMechanism->controlledStep(reverse);
}

void DrawerController::testModeUpdate() {
  this->updatePushPullMechanism();
  this->pushPullMechanism->sweepStep();
}
