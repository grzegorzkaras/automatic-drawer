#pragma once

class IPushPullMechanism {
public:
  virtual void initialize() = 0;

  virtual int getMaxRangeLimit() const = 0;

  virtual void setSpeed(const int speed) = 0;
  virtual void setRangeLimit(const int backLimit, const int frontLimit) = 0;

  virtual void sweepStep() = 0;
  virtual void controlledStep(const bool reverse) = 0;

  virtual ~IPushPullMechanism() {}
};