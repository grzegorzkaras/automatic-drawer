#pragma once

class IStatusIndicator {
public:
  virtual void activate() = 0;
  virtual void deactivate() = 0;

  virtual ~IStatusIndicator() {}
};