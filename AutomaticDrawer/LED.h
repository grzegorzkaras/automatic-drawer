#pragma once

#include <Arduino.h>

#include "IStatusIndicator.h"

class LED : virtual public IStatusIndicator {
private:
  const int pin;

public:
  LED(const int pin);
  
  virtual void activate();
  virtual void deactivate();
};
