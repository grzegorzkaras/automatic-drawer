#include "ProximitySensor.h"

ProximitySensor::ProximitySensor(const int pin) : pin(pin) {
  pinMode(pin, INPUT);
}

bool ProximitySensor::getStatus() {
  return digitalRead(this->pin);
}
