#include "SpeedPotentiometer.h"

SpeedPotentiometer::SpeedPotentiometer(const int pin) : pin(pin) {
  pinMode(pin, INPUT);
}

int SpeedPotentiometer::read(const int maxResolution) {
  return map(analogRead(this->pin), 0, 1023, 0, maxResolution);
}
