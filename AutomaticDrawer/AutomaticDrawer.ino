#define TEST false

#define LED_DIODE 2
#define SPEED_POTENTIOMETER A5
#define RANGE_POTENTIOMETER A4
#define PROXIMITY_SENSOR 4
#define SERVOMECHANISM 10

#include <Servo.h>

#include "IStatusIndicator.h"
#include "ISpeedRegulator.h"
#include "IRangeRegulator.h"
#include "IDeviceController.h"
#include "IPushPullMechanism.h"

#include "LED.h"
#include "SpeedPotentiometer.h"
#include "RangePotentiometer.h"
#include "ProximitySensor.h"
#include "Servomechanism.h"

#include "DrawerController.h"

IStatusIndicator* const led = new LED(LED_DIODE);
ISpeedRegulator* const speedPotentiometer = new SpeedPotentiometer(SPEED_POTENTIOMETER);
IRangeRegulator* const rangePotentiometer = new RangePotentiometer(RANGE_POTENTIOMETER);
IDeviceController* const proximitySensor = new ProximitySensor(PROXIMITY_SENSOR);
IPushPullMechanism* const servomechanism = new Servomechanism(SERVOMECHANISM, 35, 145);

DrawerController controller(led, speedPotentiometer, rangePotentiometer, proximitySensor, servomechanism);

void setup() {
  controller.initialize();
}

void loop() {
  #if !TEST
    controller.update();
  #else
    controller.testModeUpdate();
  #endif
}
