#include "Servomechanism.h"

Servomechanism::Servomechanism(const int pin, const int minAngle, const int maxAngle, const int delayPerAngle, const int initializeDelay, const int minPulseWidth, const int maxPulseWidth)
  : pin(pin), minAngle(minAngle), maxAngle(maxAngle), delayPerAngle(delayPerAngle), initializeDelay(initializeDelay), minPulseWidth(minPulseWidth), maxPulseWidth(maxPulseWidth) {
  this->additionalBackLimit = 0;
  this->additionalFrontLimit = 0;
  this->reverse = false;
}

int Servomechanism::getFrontLimit() const {
  return this->maxAngle - this->additionalFrontLimit;
}

int Servomechanism::getBackLimit() const {
  return this->minAngle + this->additionalBackLimit;
}

bool Servomechanism::canGoForward(const int additionalAngle) const {
  return (this->angle + additionalAngle) <= this->getFrontLimit();
}

bool Servomechanism::canGoBackward(const int additionalAngle) const {
  return (this->angle - additionalAngle) >= this->getBackLimit();
}

void Servomechanism::writeAngle() {
  if (this->lastAngle != this->angle) {
    int angle = constrain(this->angle, this->minAngle, this->maxAngle);
    
    this->servo.write(angle);
    
    this->lastAngle = this->angle;
    delay(this->delayPerAngle);
  }
}

void Servomechanism::initialize() {
  this->angle = this->getBackLimit();

  this->servo.write(this->angle);
  this->servo.attach(this->pin, this->minPulseWidth, this->maxPulseWidth);
  this->servo.write(this->angle);

  this->lastAngle = this->angle;
  delay(this->initializeDelay);
}

int Servomechanism::getMaxRangeLimit() const {
  return this->maxAngle - this->minAngle;
}

void Servomechanism::setSpeed(const int speed) {
  this->delayPerAngle = speed;
}

void Servomechanism::setRangeLimit(const int backLimit, const int frontLimit) {
  this->additionalBackLimit = backLimit;
  this->additionalFrontLimit = frontLimit;
}

void Servomechanism::sweepStep() {
  if (!this->canGoForward(1)) {
    this->reverse = true;
  } else if (!this->canGoBackward(1)) {
    this->reverse = false;
  }
  
  this->angle += this->reverse ? -1 : 1;
  
  this->writeAngle();
}

void Servomechanism::controlledStep(const bool reverse) {
  if (!reverse) {
    if (this->canGoForward(1)) {
      this->angle++;
    }
  } else {
    if (this->canGoBackward(1)) {
      this->angle--;
    }
  }
  
  this->writeAngle();
}
