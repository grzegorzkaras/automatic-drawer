# Automatic Drawer

An old prototyping platform for testing servos power, rebuilt into a device for opening a drawer using a proximity sensor. It uses a scotch yoke mechanism to perform a linear movement, which has been halved due to bidirectional servo control.